var jsUtils;

(function (undefined) {

    function getProp (o, path) {
        var p = path.split(".");

        while (p.length && o !== undefined) {
            o = o[p.shift()];
        } return o;
    }
    
    function setProp (o, path, val) {
        var object = o,
            p      = path.split("."),
            prop   = p.pop();
        
        while(p.length && o !== undefined) {
            o = o[p.shift()];
        }

        o[prop] = val;

        return object;
    }

    jsUtils = {
        getProp : getProp,
        setProp : setProp
    };

}());
